<?php

/**
 * @file
 * Functions relating to data import.
 */

/**
 * Page callback for importing module data.
 *
 * Load a CSV data file and create/update uw_ct_drupal_modules with the data.
 */
function uw_ct_drupal_modules_import() {
  $count = 0;
  if (($handle = fopen(__DIR__ . '/d8-modules.csv', 'r')) !== FALSE) {
    while (($data = fgetcsv($handle, 0, "\t")) !== FALSE) {
      $keys = [
        'module',
        'status',
        'purpose',
        'implementation',
        'testing',
        'sites',
      ];
      $data += [NULL, NULL, NULL, NULL, NULL, NULL];
      $data = array_combine($keys, $data);
      $data['sites'] = (int) $data['sites'];

      dsm($data['module']);

      $module = uw_ct_drupal_modules_module_load($data['module']);

      if (!$module) {
        $module = new stdClass();
        $module->type = 'uw_ct_drupal_modules';
        $module->promote = FALSE;
        node_object_prepare($module);

        $module->field_machine_name['und'][0]['value'] = $data['module'];
        $module->field_project_url['und'][0]['url'] = 'https://www.drupal.org/project/' . $data['module'];

        $module->field_availability['und'] = [];
        $module->field_description_use['und'][0]['value'] = NULL;
        $module->field_description_use['und'][0]['format'] = 'uw_tf_basic';
        $module->field_security_coverage['und'] = [];
      }

      // Status -> field-availability.
      $statuses = [
        'none' => '8dev',
        'dev' => '8dev',
        'alpha' => '8beta',
        'beta' => '8beta',
        'rc' => '8beta',
        'full' => 8,
        'CORE' => '8core',
      ];
      foreach ($statuses as $search => $status) {
        if (strpos($data['status'], $search) !== FALSE) {
          // We cannot use array_search() because it searches substrings:
          // '8beta' will be found for '8'.
          foreach ($module->field_availability['und'] as $item) {
            if ($item['value'] == $status) {
              // If search succeeds, nothing to be done, leave outer foreach.
              break 2;
            }
          }
          $module->field_availability['und'][] = ['value' => $status];
          break;
        }
      }

      // Note D8 security support if applicable.
      if (!isset($module->field_security_coverage['und'])) {
        $module->field_security_coverage['und'] = [];
      }
      if (strpos($data['status'], 'sec') !== FALSE && array_search(8, array_column($module->field_security_coverage['und'], 'value')) === FALSE) {
        $module->field_security_coverage['und'][] = ['value' => 8];
      }

      // Implementation potential -> some values map to
      // field_drupal_8_implementation_po[und] ; others add as prose to
      // description-use.
      $mapping = [
        'Could be useful' => 5,
        'Avoid' => 6,
        'No current need' => 6,
        'Could be very useful' => 4,
        'Less likely to be useful' => 6,
        'Likely to be useful' => 4,
        'Unlikely to be useful' => 6,
      ];
      // "Not reviewed".
      $implementation = 1;
      foreach ($mapping as $from => $to) {
        if (strpos($data['implementation'], $from) !== FALSE) {
          $implementation = $to;
          $data['implementation'] = trim(preg_replace('/' . $from . '/', '', $data['implementation']));
          break;
        }
      }
      $module->field_drupal_8_implementation_po['und'][0]['value'] = $implementation;

      // Purpose / use -> description-use.
      $purpose = trim($data['purpose'] . ' ' . $data['implementation']);
      // If there is a $purpose and it isn't already in field_description_use...
      if ($purpose && strpos($module->field_description_use['und'][0]['value'], $purpose) === FALSE) {
        $module->field_description_use['und'][0]['value'] .= ' ' . $purpose;
      }

      // Testing -> field_testing.
      if ($data['testing']) {
        $module->field_testing['und'][0]['value'] = $data['testing'];
      }

      // Sites -> field_in_use_on_sites.
      $module->field_in_use_on_sites['und'][0]['value'] = $data['sites'];

      // For testing.
      if (FALSE) {
        node_save($module);
      }

      dsm($module);
      dsm($data);

      // For testing.
      if (FALSE && $count++ == 2) {
        break;
      }
    }
    fclose($handle);
  }

  return 'uw_ct_drupal_modules_import';
}

/**
 * Load a module by machine name.
 *
 * @param string $machine_name
 *   The machine name of the moduel to load.
 *
 * @return object|null
 *   The node object or NULL if there is no node.
 */
function uw_ct_drupal_modules_module_load($machine_name) {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'uw_ct_drupal_modules')
    ->fieldCondition('field_machine_name', 'value', $machine_name, NULL, NULL, 'und');
  $result = $query->execute();
  if (isset($result['node'])) {
    $nid = key($result['node']);
    return node_load($nid);
  }
}
