<?php

/**
 * @file
 * Functions relating to data updates.
 */

/**
 * Page callback for updating module data from drupal.org.
 *
 * Updates the human-readable name for each module.
 */
function uw_ct_drupal_modules_update() {
  foreach (uw_ct_drupal_modules_get_module_list() as $nid) {
    $node = node_load($nid, NULL, TRUE);
    $module_machine_name = $node->field_machine_name['und'][0]['value'];
    $name = uw_ct_drupal_modules_get_module_title($module_machine_name);
    if ((string) $name !== '' && $node->title !== $name) {
      $node->title = $name;
      drupal_set_message(t('Updated %module_machine_name to %module_name.', ['%module_machine_name' => $module_machine_name, '%module_name' => $name]));
      node_save($node);
    }
  }

  return 'uw_ct_drupal_modules_update';
}

/**
 * Return nid's of all nodes of type uw_ct_drupal_modules.
 *
 * @return intp[]
 *   An array of nid.
 */
function uw_ct_drupal_modules_get_module_list() {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'uw_ct_drupal_modules');
  $result = $query->execute();
  return array_keys($result['node']);
}

/**
 * Return the human-readable title of a module.
 *
 * @param string $module_name
 *   The name of the module.
 *
 * @return string
 *   The human-readable title of a module.
 */
function uw_ct_drupal_modules_get_module_title($module_name) {
  $data = uw_ct_drupal_modules_get_module_info_core($module_name);
  if (empty($data->data['title'])) {
    $data = uw_ct_drupal_modules_get_module_info_core($module_name, 8);
  }
  return $data->data['title'];
}

/**
 * Return data about a module.
 *
 * @param string $module_name
 *   The name of the module.
 * @param int $core_version
 *   The Drupal major version about which to get data.
 *
 * @return array
 *   An array of data about the module.
 */
function uw_ct_drupal_modules_get_module_info_core($module_name, $core_version = NULL) {
  module_load_include('inc', 'update', 'update.fetch');

  $cid = 'available_releases::';
  if ($core_version) {
    $cid .= $core_version . '::';
  }
  $cid .= $module_name;

  // If data about this module is already cached, return it.
  $data = _update_cache_get($cid);
  if ($data) {
    return $data;
  }

  // If not cached, load the data into the cache.
  $project = [
    'name' => $module_name,
        // Default values to prevent undefined index errors.
    'project_type' => NULL,
    'includes' => [],
  ];
  if ($core_version) {
    _uw_ct_drupal_modules_update_process_fetch_task($project, $core_version);
  }
  else {
    _update_process_fetch_task($project);
  }
  // Return data from cache.
  return _update_cache_get($cid);
}

/**
 * A version of _update_process_fetch_task() with $core_version.
 *
 * Changes _update_process_fetch_task() so that if $core_version is specified,
 * that is used in the URL instead of the default and the core is set in the
 * $cid. The changes are noted below.
 *
 * @param array $project
 *   Associative array of information about the project to fetch data for.
 * @param int $core_version
 *   The Drupal major version about which to get data.
 *
 * @return bool
 *   TRUE if we fetched parsable XML, otherwise FALSE.
 */
function _uw_ct_drupal_modules_update_process_fetch_task(array $project, $core_version) {
  global $base_url;
  $fail = &drupal_static(__FUNCTION__, array());
  // This can be in the middle of a long-running batch, so REQUEST_TIME won't
  // necessarily be valid.
  $now = time();
  if (empty($fail)) {
    // If we have valid data about release history XML servers that we have
    // failed to fetch from on previous attempts, load that from the cache.
    if (($cache = _update_cache_get('fetch_failures')) && ($cache->expire > $now)) {
      $fail = $cache->data;
    }
  }

  $max_fetch_attempts = variable_get('update_max_fetch_attempts', UPDATE_MAX_FETCH_ATTEMPTS);

  $success = FALSE;
  $available = array();
  $site_key = drupal_hmac_base64($base_url, drupal_get_private_key());
  // Removed:
  // @code
  // $url = _update_build_fetch_url($project, $site_key);
  // @endcode
  $fetch_url_base = _update_get_fetch_url_base($project);
  $project_name = $project['name'];
  // Added:
  $url = $fetch_url_base . '/' . $project_name . '/' . $core_version . '.x';

  if (empty($fail[$fetch_url_base]) || $fail[$fetch_url_base] < $max_fetch_attempts) {
    $xml = drupal_http_request($url);
    if (!isset($xml->error) && isset($xml->data)) {
      $data = $xml->data;
    }
  }

  if (!empty($data)) {
    $available = update_parse_xml($data);
    // @todo: Purge release data we don't need (http://drupal.org/node/238950).
    if (!empty($available)) {
      // Only if we fetched and parsed something sane do we return success.
      $success = TRUE;
    }
  }
  else {
    $available['project_status'] = 'not-fetched';
    if (empty($fail[$fetch_url_base])) {
      $fail[$fetch_url_base] = 1;
    }
    else {
      $fail[$fetch_url_base]++;
    }
  }

  $frequency = variable_get('update_check_frequency', 1);
  // Changed by added $core_version:
  $cid = 'available_releases::' . $core_version . '::' . $project_name;
  _update_cache_set($cid, $available, $now + (60 * 60 * 24 * $frequency));

  // Stash the $fail data back in the DB for the next 5 minutes.
  _update_cache_set('fetch_failures', $fail, $now + (60 * 5));

  // Whether this worked or not, we did just (try to) check for updates.
  variable_set('update_last_check', $now);

  // Now that we processed the fetch task for this project, clear out the
  // record in {cache_update} for this task so we're willing to fetch again.
  _update_cache_clear('fetch_task::' . $project_name);

  return $success;
}
